/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2015, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONException;
import org.json.JSONObject;
import org.ubhave.intelligenttrigger.learners.ActivityLearner;
import org.ubhave.intelligenttrigger.learners.InterruptibilityLearner;
import org.ubhave.intelligenttrigger.learners.Learner;
import org.ubhave.intelligenttrigger.learners.LocationLearner;
import org.ubhave.intelligenttrigger.triggers.SensorTrigger;
import org.ubhave.intelligenttrigger.triggers.TimeTrigger;
import org.ubhave.intelligenttrigger.triggers.Trigger;
import org.ubhave.intelligenttrigger.triggers.config.BasicTriggerConfig;
import org.ubhave.intelligenttrigger.triggers.definitions.TriggerDefinition;
import org.ubhave.intelligenttrigger.utils.Constants;
import org.ubhave.intelligenttrigger.utils.ITException;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.ESSensorManager;
import com.ubhave.sensormanager.SensorDataListener;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.triggermanager.ESTriggerManager;
import com.ubhave.triggermanager.TriggerException;

import si.uni_lj.fri.lrss.machinelearningtoolkit.MachineLearningManager;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;

/**
 * The manager instantiates triggers and ensures that they live through the
 * application destruction and re-creation. A trigger can be either time based
 * or context (sensor) based. The first are fired according to the given time
 * configuration (e.g. every day, random times, etc.), the second are fired when
 * sensing data satisfies a given condition (location == home, user interruptible, etc).
 *
 * Sensor-based trigger conditions are given with a modality and a relationship to the target
 * value. For example, "activity", "==", "still". There is a special modality
 * called "interruptibility" which indicates that a user can be interrupted. It
 * is inferred from location, acceleration and time of day/weekend features.
 *
 * The Learner is a wrapper for the classifier and feature extraction logic. One
 * learner corresponds to one modality, but can use multiple sensors. The
 * interruptibility learner, for example, uses time, accelerometer and location.
 *
 * A learner can be trained from the listener feedback. Instances on which
 * decisions to fire the trigger or not are kept within a learner together with
 * a reference. The listener is passed the reference of the instance that
 * resulted in a trigger event. The same reference can be passed from the
 * listener, together with the actual outcome, to the learner, and the learner
 * can then train its classifier(s) on it.
 *
 * The Manager has methods that allow it to save classifiers to a file, and load
 * them from a file. This should be used when a service that uses the manager is
 * (re)started/destroyed.
 *
 * Only one IntelligentTriggerManager exists per context to ensure consistency.
 *
 * LIMITATIONS: 
 *
 * - Saving state: ideally, both triggers (e.g. notify the listener when the 
 * user is at home) and learners (e.g. GPS coords of home/work) should be
 * saved. However, the library requires outside listeners right now, and those
 * listeners cannot be preserved within the library json file. We can go around
 * this by changing the library to use Intents and actions instead of explicit
 * notifications. Still, TriggerManager uses direct listeners, so we should
 * probably modify that library as well. Therefore, for now we save the
 * learners only. The overlying app has to take care of trigger reinstantiation.
 *
 * - TriggerManager (Cambridge library) maintains its own track of fired triggers.
 * However, these need not be the same as the triggers that actually get delivered,
 * as we do tests of whether triggers are paused later in the pipeline. Consequently, TriggerManager
 * can consider that a trigger has been fired even if that's not the case.
 *
 * @author Veljko Pejovic, University of Birmingham, UK
 *         <v.pejovic@cs.bham.ac.uk>
 *
 */
public class IntelligentTriggerManager implements SensorDataListener {

    private static final String TAG = "IntelligentTriggerMngr";

    private static IntelligentTriggerManager mIntelligentTriggerManager;

    private static final Object lock = new Object();

    private final Context mContext;

    private ESTriggerManager mTriggerManager;

    private ESSensorManager mSensorManager;

    // triggers that we have: triggerID - Trigger
    private ConcurrentHashMap<String, Trigger> mTriggerMap;

    // mods that we learn: modality - Learner
    private HashMap<Integer, Learner> mModLearnerMap;

    // List of triggers that use the given sensor: SensorID - Trigger[]
    private SparseArray<ArrayList<String>> mSensorTriggerMap;

    // Subscription ref for each sensor: SensorID - subscription
    private SparseIntArray mSensorSbscrMap;

    // Whether a subscription is running: subscription - running
    private SparseArray<Boolean> mSbscrRunningMap;

    // The latest data for each of the sensors: SensorID - data
    private SparseArray<SensorData> mSensorData;

    /**
     * Instantiates the singleton IntelligentTriggerManager class.
     *
     * @param context A unique context upon which the class is instantiated.
     * @throws ITException
     */
    public IntelligentTriggerManager(Context context) throws ITException {

        mContext = context;

        try {
            // Instantiate Cambridge's libraries
            mTriggerManager = ESTriggerManager.getTriggerManager(mContext);
            mSensorManager = ESSensorManager.getSensorManager(mContext);

            if (Arrays.asList(mContext.fileList()).contains(
                    Constants.LEARNER_STORAGE_FILE)) {
                loadFromPersistent();
            } else {
                mModLearnerMap = new HashMap<Integer, Learner>();
            }

            mTriggerMap = new ConcurrentHashMap<String, Trigger>();

            mSensorTriggerMap = new SparseArray<ArrayList<String>>();

            mSensorSbscrMap = new SparseIntArray();

            mSbscrRunningMap = new SparseArray<Boolean>();

            mSensorData = new SparseArray<SensorData>();

        } catch (TriggerException | ESException e) {
            throw new ITException(ITException.INVALID_CONTEXT,
                    "Invalid context supplied.");
        }
    }

    /**
     * Return the IntelligentTriggerManager for the given context.
     * Instantiate the IntelligentTriggerManager, if needed.
     *
     * @param context A unique context upon which the class is instantiated.
     * @return A singleton IntelligentTriggerManager instance.
     * @throws ITException
     */
    public static IntelligentTriggerManager getTriggerManager(Context context) throws ITException {
        if (mIntelligentTriggerManager == null) {
            synchronized (lock) {
                mIntelligentTriggerManager = new IntelligentTriggerManager(context);
            }
        }
        return mIntelligentTriggerManager;
    }

    /**
     * Get the latest recorded data for the given sensor type.
     *
     * @param sensorID Sensor type.
     * @return The latest sensed data bundle.
     * @throws ITException
     */
    public SensorData getSensorData(int sensorID) throws ITException {
        return mSensorData.get(sensorID);
    }

    /**
     * Gets a learner that corresponds to the given modality. Adds the learner
     * to the map of instantiated learners, if it is not already present.
     *
     * @param modID An integer code for a modality: 1:time, 2:activity, 3:location
     * 4:emotion (not implemented yet), 5:interruptibility.
     *
     * @return The learner for the given modality.
     * @throws MLException
     * @throws ITException
     */
    public Learner getLearner(int modID) throws ITException {

        Learner l = mModLearnerMap.get(modID);
        if (mModLearnerMap.get(modID) == null) {
            switch (modID) {
                case (Constants.MOD_ACTIVITY):
                    l = new ActivityLearner();
                    break;
                case (Constants.MOD_INTERRUPTIBILITY):
                    l = new InterruptibilityLearner();
                    break;
                case (Constants.MOD_LOCATION):
                    l = new LocationLearner();
                    break;
                default:
                    throw new ITException(ITException.UNKNOWN_MODALITY,
                            "Unknown modality: " + modID);
            }

            try {
                l.prepareLearner(mContext);
            } catch (MLException e) {
                e.printStackTrace();
                throw new ITException(ITException.UNKNOWN_ERROR,
                        "Check stack trace printout.");
            }
            mModLearnerMap.put(modID, l);
        }
        return l;
    }

    /**
     * Check if the manager already knows about, and manages the given trigger ID.
     *
     * @param ID A string ID (name) of the trigger. Needs to be unique.
     * @return True if the trigger is managed by the manager.
     */
    public boolean triggerExists(String ID) {
        return mTriggerMap.containsKey(ID);
    }

    /**
     * Start managing a trigger with the given name, definition and callback receiver.
     *
     * @param ID A unique trigger name.
     * @param def A trigger definition (configuration).
     * @param receiver A receiver which will be notified once
     * triggering conditions are satisfied.
     * @throws ITException
     */
    public void addIntelligentTrigger(String ID,
                                      TriggerDefinition def,
                                      IntelligentTriggerReceiver receiver)
            throws ITException{

        if (mTriggerMap.containsKey(ID)) {
            // TODO: throwing an exception might be too harsh?
            throw new ITException(ITException.TRIGGER_EXISTS,
                    "Trigger with the name " + ID + " already exists.");
        }

        Trigger trigger;

        int triggerType = def.getTriggerType();

        switch (triggerType) {
            case (Constants.TIME_TRIGGER):
                try {
                    trigger = new TimeTrigger(ID, def, receiver, mContext, this);
                } catch (TriggerException e) {
                    e.printStackTrace();
                    throw new ITException(ITException.UNKNOWN_ERROR,
                            "Check stack trace printout.");
                }
                break;
            case (Constants.SENSOR_TRIGGER):
                try {
                    trigger = new SensorTrigger(ID, def, receiver, mContext,
                            this);
                } catch (ESException e) {
                    e.printStackTrace();
                    throw new ITException(ITException.UNKNOWN_ERROR,
                            "Check stack trace printout.");
                }
                break;
            default:
                throw new ITException(ITException.UNKNOWN_TRIGGER_TYPE,
                        "Unknown trigger type " + triggerType);
        }

        mTriggerMap.put(ID, trigger);
    }

    /**
     * Return a trigger object corresponding to the given ID.
     * @param ID A string trigger ID.
     * @return A trigger object.
     */
    public Trigger getTrigger(String ID) {
        if (mTriggerMap.containsKey(ID)) {
            return mTriggerMap.get(ID);
        } else {
            return null;
        }
    }

    /**
     * Change the target value of a triggering condition of a sensor trigger.
     *
     * @param ID A trigger ID.
     * @param mod A modality whose value needs to be changed.
     * @param value A new value that the condition is changing to.
     * @throws ITException
     */
    public void changeConditionValue(String ID, int mod, Object value) throws ITException {
        if (this.triggerExists(ID)) {
            Trigger trigger = mTriggerMap.get(ID);
            if (trigger.triggerType() == Constants.SENSOR_TRIGGER) {
                ((SensorTrigger) trigger).changeConditionValue(mod, value);
            }
        } else throw new ITException(ITException.TRIGGER_DOES_NOT_EXIST,
                "Unknown trigger ID " + ID);
    }

    /**
     * Change the configuration of a trigger. This method only changes the existing
     * config parameter values, and adds new parameters and their values. If a config
     * parameter needs to be completely omitted from the new trigger configuration,
     * the trigger needs to be removed, and a new one needs to be instantiated.
     *
     * @param ID ID of a trigger.
     * @param config Triggering configuration object.
     * @throws ITException
     */
    public void changeConfig(String ID, BasicTriggerConfig config) throws ITException {
        if (this.triggerExists(ID)) {
            Trigger trigger = mTriggerMap.get(ID);
            trigger.changeConfig(config);
        } else throw new ITException(ITException.TRIGGER_DOES_NOT_EXIST,
                "Unknown trigger ID " + ID);
    }

    /**
     * Pauses a trigger. No callbacks will be received from this trigger. In case
     * of a sensing trigger, no sensing will be performed for this trigger while
     * it is paused.
     *
     * @param ID String ID of the trigger.
     * @throws ITException
     */
    public void pauseIntelligentTrigger(String ID) throws ITException {

        Log.d(TAG, "Pause trigger " + ID);

        if (this.triggerExists(ID)) {

            Trigger trigger = mTriggerMap.get(ID);

            trigger.pauseTrigger();

            // For sensor triggers:
            if (trigger.triggerType() == Constants.SENSOR_TRIGGER) {

                for (int i = 0; i < mSensorTriggerMap.size(); i++) {

                    int key = mSensorTriggerMap.keyAt(i);

                    ArrayList<String> sensorTriggerList = mSensorTriggerMap
                            .get(key);

                    if (sensorTriggerList.contains(ID)) {

                        // Check if there are any active triggers for this
                        // sensor
                        boolean running = false;

                        for (String iterTriggerID : sensorTriggerList) {
                            running = running || mTriggerMap.get(iterTriggerID).isRunning();
                        }

                        // If this was the last active trigger to use this sensor
                        // pause the subscription.
                        if (!running) {
                            try {
                                int subscription = mSensorSbscrMap.get(key);
                                Log.d(TAG, "Pause subscription");
                                mSensorManager.pauseSubscription(subscription);
                                // mSensorManager.unsubscribeFromSensorData(subscription);
                                mSbscrRunningMap.put(subscription, false);
                            } catch (ESException e) {
                                e.printStackTrace();
                                throw new ITException(ITException.UNKNOWN_ERROR,
                                        "Check stack trace printout.");
                            }
                        }
                    }
                }
            }
        } else {
            throw new ITException(ITException.TRIGGER_DOES_NOT_EXIST,
                    "Trigger " + ID + " does not exist.");
        }
    }

    /**
     * Unpause trigger. In case of a sensing trigger, sensing is now reinstantiated.
     *
     * @param ID A string ID of the trigger.
     * @throws ITException
     */
    public void unpauseIntelligentTrigger(String ID) throws ITException {

        Log.d(TAG, "Unpause trigger " + ID);

        if (this.triggerExists(ID)) {

            Trigger trigger = mTriggerMap.get(ID);

            trigger.unpauseTrigger();

            // For sensor triggers:
            if (trigger.triggerType() == Constants.SENSOR_TRIGGER) {

                int key = 0;

                for (int i = 0; i < mSensorTriggerMap.size(); i++) {
                    key = mSensorTriggerMap.keyAt(i);
                    ArrayList<String> sensorTriggerList = mSensorTriggerMap
                            .get(key);

                    if (sensorTriggerList.contains(ID)) {
                        try {
                            int subscription = mSensorSbscrMap.get(key);
                            mSensorManager.unPauseSubscription(subscription);
                            Log.d(TAG, "UnPause subscription "+key+" "+subscription);
                            mSbscrRunningMap.put(subscription, true);
                        } catch (ESException e) {
                            e.printStackTrace();
                            throw new ITException(ITException.UNKNOWN_ERROR,
                                    "Check stack trace printout.");
                        }
                    }
                }
            }
        } else {
            throw new ITException(ITException.TRIGGER_DOES_NOT_EXIST,
                    "Trigger " + ID + " does not exist.");
        }
    }

    /**
     * Remove a trigger. In case of a sensing trigger, sensing of modalities
     * not sensed by any other trigger will be stopped.
     *
     * @param ID A string trigger ID.
     * @throws ITException
     */
    public void removeIntelligentTrigger(String ID) throws ITException {

        Log.d(TAG, "Remove trigger " + ID);

        int key = 0;

        for (int i = 0; i < mSensorTriggerMap.size(); i++) {
            key = mSensorTriggerMap.keyAt(i); // sensorID
            ArrayList<String> sensorTriggerList = mSensorTriggerMap.get(key);

            if (sensorTriggerList.contains(ID)) {
                sensorTriggerList.remove(ID);
            }

            if (sensorTriggerList.size() == 0) {
                try {
                    mSensorManager.unsubscribeFromSensorData(mSensorSbscrMap
                            .get(key));
                    Log.d(TAG, "Unsubscribe from "+key);
                    mSbscrRunningMap.remove(mSensorSbscrMap.get(key));
                    mSensorSbscrMap.removeAt(key);
                } catch (ESException e) {
                    // TODO: just a silent drop, as we want to remove the trigger anyway
                    // throw new ITException(ITException.SENSOR_TRIGGER_REMOVAL_ERROR,
                    //		"Sensor subscription removal failed.");
                }
            }
        }

        Trigger t = mTriggerMap.get(ID);
        if (t != null) {
            try {
                t.removeTrigger();
                mTriggerMap.remove(ID);
            } catch (TriggerException e) {
                throw new ITException(ITException.TIME_TRIGGER_REMOVAL_ERROR,
                        "Time trigger removal error.");
            }
        }
    }

    /**
     * Train learner with an array of labelled instances.
     * @param learnerID A learner ID: interruptibility, location, etc.
     * @param instances Labelled instances whose signature complies with the
     * classifier used in the learner. I.e. each instance has the same modalities and
     * data types as the signature of the learner's classifier.
     */
    public void trainLearner(int learnerID, ArrayList<Instance> instances) {
        Learner learner = mModLearnerMap.get(learnerID);
        learner.trainLearner(instances);
    }

    /**
     * Train learner with a single data instance.
     *
     * @param learnerID An integer leaner ID, e.g. interruptibility, location, etc.
     * @param classVal A class value label of the data instance.
     * @param featureValues Feature values of the data instance. Need to correspond
     * to the features used in the classifier of the learner.
     */
    public void trainLearner(int learnerID, Object classVal,
                             Object... featureValues) {
        Learner learner = mModLearnerMap.get(learnerID);
        learner.trainLearner(classVal, featureValues);
    }

    /**
     * Train learner with a single feedback label. The label is then assigned to
     * the instance that resulted in a positive classification of an
     * interruptibility moment. The instance is referenced via its integer ID.
     *
     * @param learnerID A learner that is being trained.
     * @param instanceID An instance that the label should be assigned to.
     * @param value A value of the label.
     * @throws ITException
     * @throws MLException
     */
    public void trainLearnerFromFeedback(int learnerID, int instanceID,
                                         Object value) throws ITException, MLException {
        Learner learner = mModLearnerMap.get(learnerID);
        if (learner == null) {
            throw new ITException(ITException.UNKNOWN_MODALITY,
                    "Unknown modality (learner)");
        }
        learner.trainFromFeedback(value, instanceID);
    }

    /**
     * Add trigger to the list of triggers that use the given sensor.
     * When data is sensed, triggers that use the specific sensor
     * are checked and fired if needed.
     *
     * @param sensorID A trigger sensor ID, needs to come from
     * com.ubhave.sensormanager.sensors.SensorUtils.
     * @param trigger A trigger name.
     * @throws ESException
     */
    public void addSensorTrigger(int sensorID, String trigger) throws ESException {
        ArrayList<String> sensorTriggerList = mSensorTriggerMap
                .get(sensorID);
        if (sensorTriggerList == null) {
            sensorTriggerList = new ArrayList<String>();
        }

        if (sensorTriggerList.size() == 0) {
            int subscription;
            subscription = mSensorManager.subscribeToSensorData(
                    sensorID, this);
            mSensorSbscrMap.put(sensorID, subscription);
            mSbscrRunningMap.put(subscription, true);
            Log.d(TAG, "Subscribed to "+sensorID);
        }

        if (!sensorTriggerList.contains(trigger)) {
            sensorTriggerList.add(trigger);
        }
        mSensorTriggerMap.put(sensorID, sensorTriggerList);
    }


    @Override
    public synchronized void onDataSensed(SensorData data) {

        mSensorData.put(data.getSensorType(), data);
        Log.d(TAG, "Data sensed from " + data.getSensorType());

        ArrayList<String> sensorTriggerList = mSensorTriggerMap.get(data
                .getSensorType());

        for (String triggerID : sensorTriggerList) {
            try {
                Trigger trigger = mTriggerMap.get(triggerID);

                if (trigger.isRunning()){
                    trigger.triggerCheckAndFire();
                }
            } catch (ITException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Returns a JSON string representation of the learners, classifiers they include,
     * and instances used for classifier training.
     *
     * @return A JSON formatted string.
     */
    public String getJSON() {

        Gson gson = new Gson();

        String JSONlearners = gson.toJson(mModLearnerMap);

        try {
            JSONObject JObject;
            JObject = new JSONObject(JSONlearners);
            MachineLearningManager mlManager = MachineLearningManager
                    .getMLManager(mContext);
            JObject.put(Constants.CLASSIFIERS,
                    new JSONObject(mlManager.getJSON()));
            return JObject.toString();
        } catch (JSONException | MLException e) {
            e.printStackTrace();
        }

        return JSONlearners;
    }

    /**
     * Saves learners, classifiers they include, and instances used
     * for classifier training to persistent internal storage.
     */
    public void saveToPersistent() {

        Log.d(TAG, "saveToPersistent");
        MachineLearningManager mlManager;
        try {
            mlManager = MachineLearningManager.getMLManager(mContext);
            mlManager.saveToPersistent();
        } catch (MLException e1) {
            e1.printStackTrace();
        }

        Gson gson = new Gson();

        String JSONlearners = gson.toJson(mModLearnerMap);

        try {
            FileOutputStream fos = mContext.openFileOutput(
                    Constants.LEARNER_STORAGE_FILE, Context.MODE_PRIVATE);
            OutputStreamWriter osw = new OutputStreamWriter(fos);
            osw.write(JSONlearners);
            osw.flush();
            osw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

		/*
		 * String JSONtriggers = gson.toJson(mTriggerMap); try {
		 * FileOutputStream fos =
		 * mContext.openFileOutput(Constants.TRIGGER_STORAGE_FILE,
		 * Context.MODE_PRIVATE); OutputStreamWriter osw = new
		 * OutputStreamWriter(fos); osw.write(JSONtriggers); osw.flush();
		 * osw.close();
		 * 
		 * } catch (FileNotFoundException e) { e.printStackTrace(); } catch
		 * (IOException e) { e.printStackTrace(); }
		 */

    }

    /**
     * Retrieves learners, classifiers they include, and instances used
     * for classifier training from persistent internal storage.
     */
    public void loadFromPersistent() {

        Log.d(TAG, "loadFromPersistent");
        // first load the classifiers from MLmanager
        MachineLearningManager mlManager;
        try {
            mlManager = MachineLearningManager.getMLManager(mContext);
            // mlManager.loadFromPersistent(); <- This is already called when
            // you do getMLManager
        } catch (MLException e1) {
            e1.printStackTrace();
        }

        // then build learners
        StringBuilder JSONstring = new StringBuilder();

        try {
            FileInputStream is = mContext
                    .openFileInput(Constants.LEARNER_STORAGE_FILE);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null) {
                JSONstring.append(line);
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Gson gson = new GsonBuilder().registerTypeHierarchyAdapter(
                Learner.class, new LearnerAdapter()).create();

        HashMap<Integer, Learner> helper = new HashMap<Integer, Learner>();

        Type type = new TypeToken<HashMap<Integer, Learner>>() {
        }.getType();
        mModLearnerMap = (HashMap<Integer, Learner>) gson.fromJson(
                JSONstring.toString(), type);

        // Prepare learners (connect to a context):
        for (Learner l : mModLearnerMap.values()) {
            try {
                l.prepareLearner(mContext);
            } catch (MLException e) {
                e.printStackTrace();
            }
        }
    }

    static class LearnerAdapter implements JsonDeserializer<Learner> {

        Gson gson;

        LearnerAdapter() {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gson = gsonBuilder.create();
        }

        public Learner deserialize(JsonElement elem, Type a_type,
                                   JsonDeserializationContext context) throws JsonParseException {
            Learner result = null;

            JsonObject object = elem.getAsJsonObject();
            int type = object.get("mType").getAsInt();

            switch (type) {
                case Constants.MOD_ACTIVITY:
                    result = gson.fromJson(elem, ActivityLearner.class);
                    break;
                case Constants.MOD_INTERRUPTIBILITY:
                    result = gson.fromJson(elem, InterruptibilityLearner.class);
                    break;
                case Constants.MOD_LOCATION:
                    result = gson.fromJson(elem, LocationLearner.class);
                    break;
            }
            if (result != null) result.printLearnerInfo();
            return result;
        }
    }

    /**
     * Check if a trigger is paused or not.
     * @param triggerID A trigger string ID.
     * @return True if the trigger is running.
     */
    public boolean isTriggerRunning(String triggerID) {

        Trigger t = mTriggerMap.get(triggerID);

        return t != null ? t.isRunning() : false;
    }

    /**
     * Pause all triggers. Stop sensing if needed.
     */
    public void pauseAllTriggers() {
        Set<String> stringSet = mTriggerMap.keySet();
        for (String triggerID : stringSet) {
            try {
                pauseIntelligentTrigger(triggerID);
            } catch (ITException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Unpause all triggers and restart sensing if needed.
     */
    public void unpauseAllTriggers() {
        Set<String> stringSet = mTriggerMap.keySet();
        for (String triggerID : stringSet) {
            try {
                unpauseIntelligentTrigger(triggerID);
            } catch (ITException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onCrossingLowBatteryThreshold(boolean isBelowThreshold) {}

}

/*******************************************************************************
 * Copyright (c) 2013, University of Birmingham, UK
 * Veljko Pejovic,  <v.pejovic@cs.bham.ac.uk>
 * 
 * 
 * This library was developed as part of the EPSRC Ubhave (Ubiquitous and
 * SocialComputing for Positive Behaviour Change) Project. For more information,
 * please visit http://www.ubhave.org
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with
 * or without fee is hereby granted, provided that the above copyright notice and this
 * permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ******************************************************************************/
package org.ubhave.intelligenttrigger.triggers.definitions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.ubhave.intelligenttrigger.triggers.config.BasicTriggerConfig;
import org.ubhave.intelligenttrigger.utils.Constants;
import org.ubhave.intelligenttrigger.utils.ITException;

import android.util.SparseArray;

public class SensorTriggerDefinition extends TriggerDefinition {

	public static final int REL_EQUALS = 1;
	public static final int REL_NOT_EQUALS = 2;
	public static final int REL_GREATER = 3;
	public static final int REL_LESS = 4;	
	
	private SparseArray<TriggerCondition> mConditions;
	
	private BasicTriggerConfig mTriggerSubconfig;
	
	public SensorTriggerDefinition(BasicTriggerConfig config) {
		super(Constants.SENSOR_TRIGGER);
		mTriggerSubconfig = config;
		mConditions = new SparseArray<SensorTriggerDefinition.TriggerCondition>();
	}

	public void addCondition(int modality, int relationship, Object value) throws ITException {
		mConditions.put(modality, new TriggerCondition(relationship, value));
	}
	
	public void changeConditionValue(int modality, Object value) throws ITException {
		if (mConditions.indexOfKey(modality) >= 0) {
			mConditions.get(modality).setTargetValue(value);
		}
		else throw new ITException(ITException.UNKNOWN_MODALITY,
				"Unknown modality " + modality);
	}
	
	public boolean usesModality(int mod) {
		if (mConditions.get(mod) != null){
			return true;
		}		
		else {
			return false;
		}
	}
	
	public List<Integer> allModalitiesUsed() {
		ArrayList<Integer> mods = new ArrayList<Integer>();
		for(int i = 0; i< mConditions.size(); i++) {
			mods.add(mConditions.keyAt(i));
		}
		return mods;		
	}
	
	public boolean satisfiesCondition(int mod, Object value) throws ITException {
		
		if (!usesModality(mod)){
			throw new ITException(ITException.MODALITY_NOT_USED, 
					"Trigger definition does not include this modality: "+mod);
		}
		
		TriggerCondition c = mConditions.get(mod);
		
		switch (c.getRelationship()) {
			case (REL_EQUALS):
				return value.equals(c.getTargetValue());
			case (REL_NOT_EQUALS):
				return !value.equals(c.getTargetValue());
			case (REL_GREATER):
				return (Double) value > (Double) c.getTargetValue();
			case (REL_LESS):
				return (Double) value < (Double) c.getTargetValue();
			default:
				throw new ITException(ITException.UNKNOWN_RELATIONSHIP,
						"Relationship type "+c.getRelationship()+" unknown");
		}
	}	

	public void changeConfig(BasicTriggerConfig newConfig) {
		Set<String> modifiedParams = newConfig.getAllParams();
		for (String param : modifiedParams) {
			mTriggerSubconfig.addParam(param, newConfig.getParam(param));
		}		
	}
	
	public BasicTriggerConfig getSubconfig(){
		return mTriggerSubconfig;
	}
	
	private class TriggerCondition {
		
		private int mRelationship;
		private Object mValue;
		
		public TriggerCondition(int relationship, Object value)
				throws ITException {			
			if ((relationship != REL_EQUALS) &&
				(relationship != REL_GREATER) &&
				(relationship != REL_LESS) &&
				(relationship != REL_NOT_EQUALS)) {
				throw new ITException(ITException.UNKNOWN_RELATIONSHIP,
						"Relationship type "+relationship+" unknown");
			}
			
			mRelationship = relationship;
			mValue = value;
		}
		
		public int getRelationship() {
			return mRelationship;
		}
		
		public Object getTargetValue() {
			return mValue;
		}
		
		public void setTargetValue(Object a_value) {
			mValue = a_value;
		}
	}
}

/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2015, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger.triggers;

import org.ubhave.intelligenttrigger.IntelligentTriggerManager;
import org.ubhave.intelligenttrigger.IntelligentTriggerReceiver;
import org.ubhave.intelligenttrigger.triggers.config.BasicTriggerConfig;
import org.ubhave.intelligenttrigger.utils.ITException;

import com.ubhave.triggermanager.TriggerException;

import android.content.Context;

public abstract class Trigger {

	protected int mType;
	protected String mTriggerID;
	protected Context mContext;
	protected IntelligentTriggerReceiver mReceiver;
	protected IntelligentTriggerManager mManager;
	protected boolean mRunning;
	
	public Trigger(String triggerID, IntelligentTriggerReceiver receiver, Context context,
                   IntelligentTriggerManager manager) {
		mReceiver = receiver;
		mTriggerID = triggerID;
		mContext = context;
		mManager = manager;
	}
	
	public abstract void removeTrigger() throws TriggerException ;
	
	public abstract boolean triggerCheckAndFire() throws ITException;
	
	
	public void pauseTrigger() {
		mRunning = false;
	}

	public void unpauseTrigger() {
		mRunning = true;
	}

	public boolean isRunning() {
		return mRunning;
	}
	
	public abstract void changeConfig(BasicTriggerConfig newConfig);
	
	public int triggerType() {
		return mType;
	}
	
}

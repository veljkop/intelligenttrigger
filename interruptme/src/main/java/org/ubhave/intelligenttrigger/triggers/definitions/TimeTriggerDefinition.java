/*******************************************************************************
 * Copyright (c) 2013, University of Birmingham, UK
 * Veljko Pejovic,  <v.pejovic@cs.bham.ac.uk>
 * 
 * 
 * This library was developed as part of the EPSRC Ubhave (Ubiquitous and
 * SocialComputing for Positive Behaviour Change) Project. For more information,
 * please visit http://www.ubhave.org
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with
 * or without fee is hereby granted, provided that the above copyright notice and this
 * permission notice appear in all copies.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ******************************************************************************/
package org.ubhave.intelligenttrigger.triggers.definitions;

import java.util.Set;

import org.ubhave.intelligenttrigger.triggers.config.BasicTriggerConfig;
import org.ubhave.intelligenttrigger.utils.Constants;

import com.ubhave.triggermanager.config.TriggerConfig;

public class TimeTriggerDefinition extends TriggerDefinition {

	private int mTriggerSubtype; // from TriggerUtils

	private TriggerConfig mTriggerSubconfig;

	// new constructor
	public TimeTriggerDefinition(int a_triggerSubtype, BasicTriggerConfig a_config) {
		super(Constants.TIME_TRIGGER);
		mTriggerSubconfig = new TriggerConfig();
		mTriggerSubtype = a_triggerSubtype;
		changeConfig(a_config);
	}
	
	public void changeConfig(BasicTriggerConfig a_newConfig) {
		Set<String> modifiedParams = a_newConfig.getAllParams();
		for (String param : modifiedParams) {
			mTriggerSubconfig.addParameter(param, a_newConfig.getParam(param));
		}		
	}
	
	public int getTriggerSubtype(){
		return mTriggerSubtype;
	}
	public TriggerConfig getSubconfig() {
		return mTriggerSubconfig;
	}
}

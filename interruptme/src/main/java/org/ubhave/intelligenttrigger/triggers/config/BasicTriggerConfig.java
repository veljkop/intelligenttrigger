/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2015, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger.triggers.config;

import java.util.HashMap;
import java.util.Set;

import org.ubhave.intelligenttrigger.utils.Constants;

public class BasicTriggerConfig {

    private HashMap<String, Object> mParams;

    public BasicTriggerConfig(boolean init) {

        mParams = new HashMap<String, Object>();

        if (init) {
            // Add defaults
            addParam(Constants.MAX_DAILY_NOTIFICATION_CAP, Constants.DEFAULT_DAILY_NOTIFICATION_CAP);
            addParam(Constants.NUMBER_OF_NOTIFICATIONS, Constants.DEFAULT_NUMBER_OF_NOTIFICATIONS);
            addParam(Constants.DO_NOT_DISTURB_AFTER_MINUTES, Constants.DEFAULT_DO_NOT_DISTURB_AFTER_MINUTES);
            addParam(Constants.DO_NOT_DISTURB_BEFORE_MINUTES, Constants.DEFAULT_DO_NOT_DISTURB_BEFORE_MINUTES);
            addParam(Constants.MIN_TRIGGER_INTERVAL_MINUTES, 0);
        }
    }

    public void addParam(String param, Object value){
        mParams.put(param, value);
    }

    public Object getParam(String param) {
        if (mParams.containsKey(param)) {
            return mParams.get(param);
        }
        return null;
    }

    public boolean containsParam(String param) {

        return mParams.containsKey(param);
    }

    public Set<String> getAllParams() {
        return mParams.keySet();
    }
}

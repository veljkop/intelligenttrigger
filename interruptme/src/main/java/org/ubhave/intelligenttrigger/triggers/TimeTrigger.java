/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2015, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger.triggers;

import java.util.Set;

import org.ubhave.intelligenttrigger.IntelligentTriggerManager;
import org.ubhave.intelligenttrigger.IntelligentTriggerReceiver;
import org.ubhave.intelligenttrigger.triggers.config.BasicTriggerConfig;
import org.ubhave.intelligenttrigger.triggers.definitions.TimeTriggerDefinition;
import org.ubhave.intelligenttrigger.triggers.definitions.TriggerDefinition;
import org.ubhave.intelligenttrigger.utils.Constants;
import org.ubhave.intelligenttrigger.utils.ITException;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.ubhave.triggermanager.ESTriggerManager;
import com.ubhave.triggermanager.TriggerException;
import com.ubhave.triggermanager.TriggerReceiver;
import com.ubhave.triggermanager.config.TriggerConfig;
import com.ubhave.triggermanager.triggers.TriggerUtils;

public class TimeTrigger extends Trigger implements TriggerReceiver {
	
	private static final String TAG = "TimeTrigger";

	private ESTriggerManager mTriggerManager;
	
	private int mIntID;
	
	private int mTriggerSubtype;
	
	private TimeTriggerDefinition mDefinition;
	
	private TriggerConfig mTriggerSubconfig;
	
	public TimeTrigger(String ID,
			TriggerDefinition def,
			IntelligentTriggerReceiver receiver,
			Context context,
			IntelligentTriggerManager manager) throws TriggerException{
		super(ID, receiver, context, manager);

		mType = Constants.TIME_TRIGGER;

		mDefinition = (TimeTriggerDefinition) def;
		
		mTriggerSubtype = mDefinition.getTriggerSubtype();
		
		mTriggerSubconfig = mDefinition.getSubconfig();
		
		mTriggerManager = ESTriggerManager.getTriggerManager(mContext);
		
		if (mTriggerSubconfig.containsKey(Constants.MAX_DAILY_NOTIFICATION_CAP)) {
			int cap = (Integer) mTriggerSubconfig.getParameter(Constants.MAX_DAILY_NOTIFICATION_CAP);
			mTriggerManager.setNotificationCap(cap);
		}
		
		mIntID = mTriggerManager.addTrigger(mTriggerSubtype, this, mTriggerSubconfig);
		
		mRunning = true;
	}
	
	public void changeConfig(BasicTriggerConfig newConfig) {
		Set<String> modifiedParams = newConfig.getAllParams();
		for (String param : modifiedParams) {
			
			if (param.equals(Constants.MAX_DAILY_NOTIFICATION_CAP)) {
				int cap = (Integer) newConfig.getParam(param);
				mTriggerManager.setNotificationCap(cap);
			}
			
			mTriggerSubconfig.addParameter(param, newConfig.getParam(param));
			Log.d(TAG, "Modified param: "+param+" to "+ newConfig.getParam(param));
		}
		
		try {
			// Preserve running status
			removeTrigger();
			mIntID = mTriggerManager.addTrigger(mTriggerSubtype, this, mTriggerSubconfig);
		} catch (TriggerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void onNotificationTriggered(int triggerId) {
		Log.d(TAG, "onNotificationTriggered");
		if (isRunning()) {
			
			SharedPreferences settings = PreferenceManager
					.getDefaultSharedPreferences(mContext);

			long lastTriggerTimeMillis = settings.getLong(mTriggerID +"_"+Constants.LAST_TRIGGER_TIME, 0);
			
			long currentTimeMillis = System.currentTimeMillis();
			
			
			boolean result = true;
			
			if (mTriggerSubconfig.containsKey(Constants.MIN_TRIGGER_INTERVAL_MINUTES)) {
				int minInterval = (Integer) mTriggerSubconfig.getParameter(Constants.MIN_TRIGGER_INTERVAL_MINUTES);
				if ((currentTimeMillis - lastTriggerTimeMillis)/(1000*60) < minInterval ) {
					Log.d(TAG, "Min trigger interval ("+minInterval+"mins) needs to pass");
					result &= false;
				}
			}
					
			if (result) {			
				// If this is a single fire trigger, make sure it is not 
				// reinstantiated when the app restarts
				Log.d(TAG, "trigger fired");
				if (mTriggerSubtype == TriggerUtils.TYPE_CLOCK_TRIGGER_ONCE) {
					SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
					
					if (!prefs.getBoolean(mTriggerID +"_fired", false)) {
						SharedPreferences.Editor editor = prefs.edit();
						editor.putBoolean(mTriggerID +"_fired", true);
						editor.commit();				
					}
				}
				
				SharedPreferences.Editor editor  = settings.edit();
				editor.putLong(mTriggerID +"_"+Constants.LAST_TRIGGER_TIME, currentTimeMillis);
				editor.commit();
				
				mReceiver.onTriggerReceived(mTriggerID, null);
			}			
		} else {
			Log.d(TAG, "trigger paused - can't fire");
		}
		
	}
	
	public TimeTriggerDefinition getDefinition() {
		return mDefinition;
	}
	
	@Override
	public void removeTrigger() throws TriggerException {
		mTriggerManager.removeTrigger(mIntID);
	}

	@Override
	public boolean triggerCheckAndFire() throws ITException {
		// TODO Auto-generated method stub
		return false;
	}
}

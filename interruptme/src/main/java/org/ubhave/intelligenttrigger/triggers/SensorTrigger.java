/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2015, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger.triggers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.ubhave.intelligenttrigger.IntelligentTriggerManager;
import org.ubhave.intelligenttrigger.IntelligentTriggerReceiver;
import org.ubhave.intelligenttrigger.learners.Learner;
import org.ubhave.intelligenttrigger.learners.LearnerResultBundle;
import org.ubhave.intelligenttrigger.triggers.config.BasicTriggerConfig;
import org.ubhave.intelligenttrigger.triggers.definitions.SensorTriggerDefinition;
import org.ubhave.intelligenttrigger.triggers.definitions.TriggerDefinition;
import org.ubhave.intelligenttrigger.utils.Constants;
import org.ubhave.intelligenttrigger.utils.ITException;

import com.ubhave.sensormanager.ESException;
import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.triggermanager.TriggerException;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;

public class SensorTrigger extends Trigger  {

	private static final String TAG = "SensorTrigger";
	
	private SensorTriggerDefinition mDefinition;

	public SensorTrigger(String triggerID,
			TriggerDefinition def,
			IntelligentTriggerReceiver receiver,
			Context context,
			IntelligentTriggerManager manager) throws ESException, ITException {
		
		super(triggerID, receiver, context, manager);
				
		mType = Constants.SENSOR_TRIGGER;
		
		mDefinition = (SensorTriggerDefinition) def;
		
		List<Integer> modList = mDefinition.allModalitiesUsed();
		
		Iterator<Integer> iter = modList.iterator();
		
		while(iter.hasNext()) {
			int mod = iter.next();
			Learner l = mManager.getLearner(mod);
			List<Integer> sensorsUsed = l.usesSensors();
			Iterator<Integer> i = sensorsUsed.iterator();
			while (i.hasNext()) {
				int sensorID = i.next();
				mManager.addSensorTrigger(sensorID, mTriggerID);
			}
		}
		
		mRunning = true;
	}	

	public SensorTriggerDefinition getDefinition() {
		return mDefinition;
	}
	
	public void changeConditionValue(int mod, Object value) throws ITException {
		mDefinition.changeConditionValue(mod, value);
	}

	public void changeConfig(BasicTriggerConfig newConfig){
		mDefinition.changeConfig(newConfig);
	}

	// Also checks if the data is fresh
	@Override	
	public boolean triggerCheckAndFire() throws ITException {

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(mContext);

		long lastTriggerTimeMillis = settings.getLong(mTriggerID +"_"+Constants.LAST_TRIGGER_TIME, 0);
		
		long currentTimeMillis = System.currentTimeMillis();
		
		int numTriggersFired = settings.getInt(mTriggerID +"_"+Constants.TRIGGERS_FIRED_COUNT, 0);

		int lastKnownDay = settings.getInt(mTriggerID +"_"+Constants.LAST_KNOWN_DAY, 0);

		Calendar calendar = Calendar.getInstance();	
		
		int currentDay = calendar.get(Calendar.DAY_OF_YEAR);

		int currentMins = (60 * calendar.get(Calendar.HOUR_OF_DAY)) + calendar.get(Calendar.MINUTE);

		BasicTriggerConfig config = mDefinition.getSubconfig();
		
		if (lastKnownDay != currentDay) {
			numTriggersFired = 0;
		}
		
		// NOTE: we do not exit immediately after detecting that notif cap is exceeded  
		// since we want to run the learner on the sensed data for training purposes
		// e.g. for location training with interruptilbility trigger.
		
		boolean result = true;

		if (config.containsParam(Constants.MAX_DAILY_NOTIFICATION_CAP)) {
			int maxTriggers = (Integer) config.getParam(Constants.MAX_DAILY_NOTIFICATION_CAP);
			if (numTriggersFired >= maxTriggers) {
				Log.d(TAG, "Maximum notification cap exceeded for today");
				result &= false; 
			}
		}
		
		if (config.containsParam(Constants.DO_NOT_DISTURB_AFTER_MINUTES)) {
			int afterMins = (Integer) config.getParam(Constants.DO_NOT_DISTURB_AFTER_MINUTES);
			if (currentMins > afterMins) {
				Log.d(TAG, "Do not disturb time");
				result &= false;
			}
		}
		if (config.containsParam(Constants.DO_NOT_DISTURB_BEFORE_MINUTES)) {
			int beforeMins = (Integer) config.getParam(Constants.DO_NOT_DISTURB_BEFORE_MINUTES);
			if (currentMins < beforeMins) {
				Log.d(TAG, "Do not disturb time");
				result &= false;
			}
		}
		
		if (config.containsParam(Constants.MIN_TRIGGER_INTERVAL_MINUTES)) {
			int minInterval = (Integer) config.getParam(Constants.MIN_TRIGGER_INTERVAL_MINUTES);
			if ((currentTimeMillis - lastTriggerTimeMillis)/(1000*60) < minInterval ) {
				Log.d(TAG, "Min trigger interval ("+minInterval+"mins) needs to pass");
				result &= false;
			}
		}
				
		ArrayList<Object> args = new ArrayList<Object>();
		
		List<Integer> modList = mDefinition.allModalitiesUsed();
		Iterator<Integer> iterMods = modList.iterator();
		
		ArrayList<LearnerResultBundle> resultsBundle = new ArrayList<LearnerResultBundle>();
		
		while(iterMods.hasNext()) {
			int mod = iterMods.next();
			Learner l = mManager.getLearner(mod);
			
			List<Integer> sensorsUsed = l.usesSensors();
			Iterator<Integer> iterSensors = sensorsUsed.iterator();
			
			while (iterSensors.hasNext()){
				
				int sensorID = iterSensors.next();
				Log.d(TAG, "Getting recent sensor data for "+sensorID);
				
				SensorData data = mManager.getSensorData(sensorID);
				
				if (data == null){
					Log.d(TAG, "Sensor data doesn't exist, abort classification");
					return false;
				}
				
				if (System.currentTimeMillis() - data.getTimestamp() > Constants.FRESHNESS_THOLD_MILLIS) {
					Log.d(TAG, "Sensor data "+(System.currentTimeMillis() - data.getTimestamp()) + "ms too old");
					return false;
				}

				args.add(data);								
			}
			
			LearnerResultBundle resultBundle;
			try {
				resultBundle = l.classify(args);		
				if (resultBundle == null) {
					result &= false;
				} else {
					resultsBundle.add(resultBundle);
					result &= ((resultBundle.getIsTrained() || resultBundle.getIsRandom()) && 
							mDefinition.satisfiesCondition(mod, resultBundle.getValue()));
					// Only random and trained classifier data is valid.
				}
			} catch (MLException e) {
				e.printStackTrace();
				throw new ITException(ITException.UNKNOWN_ERROR,
						"Check stack trace printout.");
			}
		}
		
		if (result) {
			SharedPreferences.Editor editor  = settings.edit();
			editor.putInt(mTriggerID +"_"+Constants.LAST_KNOWN_DAY, currentDay);
			editor.putInt(mTriggerID +"_"+Constants.TRIGGERS_FIRED_COUNT, numTriggersFired + 1);
			editor.putLong(mTriggerID +"_"+Constants.LAST_TRIGGER_TIME, currentTimeMillis);
			editor.commit();
			mReceiver.onTriggerReceived(mTriggerID, resultsBundle);
		}
		
		return result;
	}
	
	@Override
	public void removeTrigger() throws TriggerException {
		// TODO Auto-generated method stub
	}
}

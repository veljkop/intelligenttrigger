/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2015, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger.utils;

public class ITException extends Exception {

	public static final int INVALID_CONTEXT = 111; 
	public static final int UNKNOWN_TRIGGER_TYPE = 104; //sensor or time
	public static final int UNKNOWN_ERROR = 112; 
	public static final int TRIGGER_EXISTS = 105;
	public static final int UNKNOWN_MODALITY = 100; 

	
	public static final int UNKNOWN_RELATIONSHIP = 101;
	public static final int INCOMPATIBLE_SENSOR_DATA = 102;
	public static final int INCOMPATIBLE_INSTANCE = 103;
	public static final int TRIGGER_DOES_NOT_EXIST = 106;
	public static final int MODALITY_NOT_USED = 107;
	public static final int SENSOR_NOT_USED = 108;
	public static final int TIME_TRIGGER_REMOVAL_ERROR = 109;
	public static final int SENSOR_TRIGGER_REMOVAL_ERROR = 110;

	private int d_errorCode;
	private String d_message;
	
	public ITException(int errorCode, String message) {
		super(message);
		this.d_message = message;
		this.d_errorCode = errorCode;
	}
	
	public int getErrorCode()
	{
		return d_errorCode;
	}

	public String getMessage()
	{
		return d_message;
	}
}

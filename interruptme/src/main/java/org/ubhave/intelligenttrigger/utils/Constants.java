/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2015, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Constants {

	public static final String CLASSIFIERS = "classifiers";
	public static final String CLASSIFIER_ACTIVITY = "activity_classifier";
	public static final String CLASSIFIER_LOCATION = "location_classifier";
	public static final String CLASSIFIER_INTERRUPTIBILITY = "interruptibility_classifier";

	public static final String TRIGGER_STORAGE_FILE = "persistent_triggers.json";
	public static final String LEARNER_STORAGE_FILE = "persistent_learner.json";

	// Preserve interface with TriggerManager
	public static final String DO_NOT_DISTURB_BEFORE_MINUTES = "limitBeforeHour";
	public static final String DO_NOT_DISTURB_AFTER_MINUTES = "limitAfterHour";
	public static final String MIN_TRIGGER_INTERVAL_MINUTES = "notificationMinInterval";
	public static final String MAX_DAILY_NOTIFICATION_CAP = "limitDailyCap";
	public final static String NUMBER_OF_NOTIFICATIONS = "numberOfNotifications";
	public final static String NOTIFICATION = "numberOfNotifications";

	public static final int DEFAULT_DO_NOT_DISTURB_BEFORE_MINUTES = 8 * 60;
	public static final int DEFAULT_DO_NOT_DISTURB_AFTER_MINUTES = 22 * 60;
	public static final int DEFAULT_MIN_TRIGGER_INTERVAL_MINUTES = 15; //TODO: change this
	public static final int DEFAULT_DAILY_NOTIFICATION_CAP = 8;
	public static final int DEFAULT_NUMBER_OF_NOTIFICATIONS = 8;
	
	public final static String LAST_TRIGGER_TIME = "lastTriggerTime";
	public final static String TRIGGERS_FIRED_COUNT = "triggersFiredCount"; // per day
	public final static String LAST_KNOWN_DAY = "lastKnownDay";

	
	public static final int MOD_TIME = 1;
	public static final int MOD_ACTIVITY = 2;
	public static final int MOD_LOCATION = 3;
	public static final int MOD_EMOTION = 4;
	public static final int MOD_INTERRUPTIBILITY = 5;

	// Expose the standard time trigger as well
	public static final int TIME_TRIGGER = 100;
	public static final int SENSOR_TRIGGER = 101;
	
	public static final long FRESHNESS_THOLD_MILLIS = 5*60*1000;

	public static final int MAX_INSTANCEQ = 1000;
	
	public static final int MIN_HOUR_WORK = 10;
	public static final int MAX_HOUR_WORK = 17;
	public static final int MIN_HOUR_HOME = 1;
	public static final int MAX_HOUR_HOME = 6;
	
	// TODO: move this to the config object
	// minimum number of location points needed before we can do inference
	// based on 20+1 min sampling for 24 hrs per day, counting in a possibility
	// of the sampling starting on friday evening (we want at least one working day).
	public static final int MIN_LOCATION_POINTS = 205;
	public static final double MAX_LOCATION_DISTANCE = 1; //km
	public static final double MIN_LOCATION_PERCENT = 20.0;

	// Define weekdays, as they vary in different societies.
	// 1:Sun, 2:Mon, ... 6:Fri, 7:Sat
	public static final List<Integer> WEEKDAYS = Arrays.asList(2,3,4,5,6);
	
	public static ArrayList<String> allConfigParams() {
		ArrayList<String> keys = new ArrayList<String>();
		keys.add(DO_NOT_DISTURB_BEFORE_MINUTES);
		keys.add(DO_NOT_DISTURB_AFTER_MINUTES);
		keys.add(MIN_TRIGGER_INTERVAL_MINUTES);
		
		return keys;
	}
}

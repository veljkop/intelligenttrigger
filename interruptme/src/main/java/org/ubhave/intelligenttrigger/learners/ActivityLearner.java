/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2013, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger.learners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.ubhave.intelligenttrigger.utils.Constants;
import org.ubhave.intelligenttrigger.utils.ITException;

import android.content.Context;
import android.util.Log;

import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.data.pullsensor.AccelerometerData;
import com.ubhave.sensormanager.sensors.SensorUtils;

import si.uni_lj.fri.lrss.machinelearningtoolkit.MachineLearningManager;
import si.uni_lj.fri.lrss.machinelearningtoolkit.classifier.Classifier;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.ClassifierConfig;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Feature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNominal;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNumeric;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Signature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Value;

public class ActivityLearner extends Learner {

	/**
	 * NOTE: This hasn't been updated for a while.  
	 * Please check it thoroughly before using this learner.
	 */
	
	
/*	public static final String ACTIVITY_SITTING = "sitting";
	public static final String ACTIVITY_STANDING = "standing";
	public static final String ACTIVITY_WALKING = "walking";	*/
	
	public static final String ACTIVITY_STILL = "still";
	public static final String ACTIVITY_ACTIVE = "active";
	
	private static final String TAG = "ActivityLearner";

	public ActivityLearner() {
		
		mType = Constants.MOD_ACTIVITY;

		mSensors = new ArrayList<Integer>();
		mSensors.add(SensorUtils.SENSOR_TYPE_ACCELEROMETER);
		
		mInstanceQ = new LinkedHashMap<Integer, Instance>(Constants.MAX_INSTANCEQ);
		
		mInstanceCounter = 0;

	}
	
	@Override
	public void prepareLearner(Context context) throws MLException {
		
		mContext = context;
		
		/*Feature x_axis = new Feature("xaxis", Feature.NUMERIC);		
		Feature y_axis = new Feature("yaxis", Feature.NUMERIC);
		Feature z_axis = new Feature("zaxis", Feature.NUMERIC);*/
		Feature mean = new FeatureNumeric("mean");
		Feature variance = new FeatureNumeric("variance");
		Feature MCR = new FeatureNumeric("MCR");
		ArrayList<String> activityValues = new ArrayList<String>();
		
		activityValues.add(ACTIVITY_STILL);
		activityValues.add(ACTIVITY_ACTIVE);
		/*activityValues.add(ACTIVITY_SITTING);
		activityValues.add(ACTIVITY_STANDING);
		activityValues.add(ACTIVITY_WALKING);*/
		Feature activity = new FeatureNominal("Activity", activityValues);
					
		ArrayList<Feature> features = new ArrayList<Feature>();		
		/*features.add(x_axis);
		features.add(y_axis);
		features.add(z_axis);*/
		
		features.add(mean);
		features.add(variance);
		features.add(MCR);
		features.add(activity);
		Signature signature = new Signature(features, features.size()-1);
		
		MachineLearningManager mlManager = MachineLearningManager.getMLManager(context);
		mlManager.addClassifier(si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants.TYPE_NAIVE_BAYES,
				signature, new ClassifierConfig(), Constants.CLASSIFIER_ACTIVITY);
		
	}

	@Override
	public void trainLearner(Object classValueO, Object... featureValuesO) {
		
		Value meanValue = new Value(featureValuesO[3], Value.NUMERIC_VALUE);
		Value varianceValue = new Value(featureValuesO[4], Value.NUMERIC_VALUE);
		Value MCRValue = new Value(featureValuesO[5], Value.NUMERIC_VALUE);
		
		Value classValue = new Value(classValueO, Value.NOMINAL_VALUE);
		
		ArrayList<Value> instanceValues = new ArrayList<Value>();
		instanceValues.add(meanValue);
		instanceValues.add(varianceValue);
		instanceValues.add(MCRValue);
		instanceValues.add(classValue);
		
		Instance instance = new Instance(instanceValues);

		ArrayList<Instance> instances = new ArrayList<Instance>();
		instances.add(instance);
		
		try {
			MachineLearningManager mlManager = MachineLearningManager.getMLManager(mContext);
			Classifier classifier = mlManager.getClassifier(Constants.CLASSIFIER_ACTIVITY);
			classifier.train(instances);
			mTrained = true;
		} catch (MLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public void trainLearner(ArrayList<Instance> instances) {
		try {
			MachineLearningManager mlManager = MachineLearningManager.getMLManager(mContext);
			Classifier classifier = mlManager.getClassifier(Constants.CLASSIFIER_ACTIVITY);
			classifier.train(instances);
			mTrained = true;
		} catch (MLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Override
	public void trainFromFeedback(Object classValueO, int instanceID) {
		if (mInstanceQ.containsKey(instanceID)) {
			Instance trainInstance = mInstanceQ.get(instanceID);
			Value classValue = new Value(classValueO, Value.NOMINAL_VALUE);
			trainInstance.addValue(classValue);
			ArrayList<Instance> instances = new ArrayList<Instance>();
			instances.add(trainInstance);
			try {
				MachineLearningManager mlManager = MachineLearningManager.getMLManager(mContext);
				Classifier classifier = mlManager.getClassifier(Constants.CLASSIFIER_ACTIVITY);
				classifier.train(instances);
				mTrained = true;
			} catch (MLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public LearnerResultBundle classify(Object... featureValuesO) throws ITException, MLException {
		
		// Accelerometer data comes in bursts. We want to vote for the 
		// most frequent outcome.
		HashMap<String, Integer> valueMap = new HashMap<String, Integer>();
		
		ArrayList<SensorData> allSensorsValues = (ArrayList<SensorData>) featureValuesO[0];
		
		SensorData featureValues = allSensorsValues.get(0); 

		if (featureValues.getSensorType() != SensorUtils.SENSOR_TYPE_ACCELEROMETER){
			throw new ITException(ITException.INCOMPATIBLE_SENSOR_DATA, 
					"Accelerometer data (type "+SensorUtils.SENSOR_TYPE_ACCELEROMETER+
					" expected, got type "+featureValues.getSensorType()+" instead.");
		}
		
		ArrayList<float[]> readings = ((AccelerometerData)featureValues).getSensorReadings();
		
		float mean = 0;
		float variance = 0;
		float MCR = 0;
		
		float totalIntensity = 0;
		float intensityList[] = new float[readings.size()];
		
		for (int i=0; i<readings.size(); i++) {
			float[] sample = readings.get(i);
			float intensity = (float) Math.sqrt(Math.pow(sample[0], 2) + Math.pow(sample[1], 2) + Math.pow(sample[2], 2));
			intensityList[i] = intensity;
			totalIntensity += intensity;
		}
		if (readings.size() > 0) {
			mean = totalIntensity/readings.size();
			
			for (int i=0; i<readings.size(); i++){
				variance += Math.pow(intensityList[i] - mean, 2);
				if (i>0) {
					if ((intensityList[i] - mean) * (intensityList[i-1] - mean) < 0){
						MCR++;
					}
				}
			}
			
			variance = variance/readings.size();
			
			if (readings.size() > 1) {
				MCR = MCR/(readings.size()-1);
			}
		}
		
		ArrayList<Value> instanceValues = new ArrayList<Value>();
		
		// MLToolkit works with double precision, thus we convert
		Value meanValue = new Value((double) mean, Value.NUMERIC_VALUE);
		Value varianceValue = new Value((double) variance, Value.NUMERIC_VALUE);
		Value MCRValue = new Value((double) MCR, Value.NUMERIC_VALUE);
		
		instanceValues.add(meanValue);
		instanceValues.add(varianceValue);
		instanceValues.add(MCRValue);
		
		Instance instance = new Instance(instanceValues);
		
		MachineLearningManager mlManager = MachineLearningManager.getMLManager(mContext);
		Classifier classifier = mlManager.getClassifier(Constants.CLASSIFIER_ACTIVITY);
		Value v = classifier.classify(instance);
		
		String strValue = (String) v.getValue();
		
		if (mInstanceQ.size() > Constants.MAX_INSTANCEQ) {
			mInstanceQ.remove(mInstanceQ.entrySet().iterator().next().getKey());
		}
		
		int instanceNumber = mInstanceCounter++;
		mInstanceQ.put(instanceNumber, instance);
		
		Log.d(TAG, "Mean: "+mean+" var: "+variance+" MCR: "+MCR+" classifed as "+strValue);
		return new LearnerResultBundle(mType, instanceNumber, mRandom, mTrained, featureValues, strValue, instance);
		
		/*for (float[] sample : readings) {

			ArrayList<Value> instanceValues = new ArrayList<Value>();

			Value xValue = new Value(sample[0], Value.NUMERIC_VALUE);
			Value yValue = new Value(sample[1], Value.NUMERIC_VALUE);
			Value zValue = new Value(sample[2], Value.NUMERIC_VALUE);
			
			instanceValues.add(xValue);
			instanceValues.add(yValue);
			instanceValues.add(zValue);
			
			Instance instance = new Instance(instanceValues);
			
			Value v = d_classifier.classify(instance);
			
			String strValue = (String) v.getValue();
			
			if (!valueMap.containsKey(strValue)){
				valueMap.put(strValue, 1);
			}
			else {
				valueMap.put(strValue, valueMap.get(strValue)+1);
			}
		}
		
		String maxValue = "";
		int maxCount = -1;
		
		Iterator<Entry<String, Integer>> iter = valueMap.entrySet().iterator();
		while (iter.hasNext()){
			Entry<String, Integer> pair = iter.next();
			String strValue = (String) pair.getKey();
			int valueCount = (Integer) pair.getValue();
			if (valueCount > maxCount) {
				maxValue = strValue;
			}
		}
		return maxValue;*/
	}


	
	@Override
	public void printLearnerInfo() {
		Log.d(TAG, "Learner type: "+ mType);
		Log.d(TAG, "Sensors used: "+ mSensors);
		Log.d(TAG, "Instances queued (size)"+ mInstanceQ.size());
		Log.d(TAG, "Instances queued (counter)"+ mInstanceCounter);
		//d_classifier.printClassifierInfo();
	}

}

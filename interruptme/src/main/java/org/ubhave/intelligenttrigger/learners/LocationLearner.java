/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2015, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger.learners;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.ubhave.intelligenttrigger.utils.Constants;
import org.ubhave.intelligenttrigger.utils.ITException;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.data.pullsensor.LocationData;
import com.ubhave.sensormanager.sensors.SensorUtils;

import si.uni_lj.fri.lrss.machinelearningtoolkit.MachineLearningManager;
import si.uni_lj.fri.lrss.machinelearningtoolkit.classifier.Classifier;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.ClassifierConfig;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Feature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNominal;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNumeric;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Signature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Value;

public class LocationLearner extends Learner {

	/**
	 * Discriminates between home and work locations. In the beginning, the
	 * learner is trained on periodically sampled location data, and the time
	 * of day. If the time of day is between MIN_HOUR_HOME and MAX_HOUR_HOME, 
	 * the location is labelled as "home". If it's between MIN_HOUR_WORK and
	 * MAX_HOUR_WORK it is labelled as "work". 
	 * After MIN_LOCATION_POINTS are collected, the learner is trained. Future
	 * queries will be answered from the trained classifier. 
	 */

	public static final String LOCATION_HOME = "home";
	public static final String LOCATION_WORK = "work";
	public static final String LOCATION_OTHER = "other";

	private static final String TAG = "LocationLearner";

	public LocationLearner() {

		mType = Constants.MOD_LOCATION;

		mSensors = new ArrayList<Integer>();

		mSensors.add(SensorUtils.SENSOR_TYPE_LOCATION);

		mInstanceQ = new LinkedHashMap<Integer, Instance>(
				Constants.MAX_INSTANCEQ);

		mInstanceCounter = 0;
	}

	public void prepareLearner(Context context) throws MLException {

		mContext = context;

		Feature longitude = new FeatureNumeric("longitude");
		Feature latitude = new FeatureNumeric("latitude");

		ArrayList<String> locValues = new ArrayList<String>();
		locValues.add(LOCATION_HOME);
		locValues.add(LOCATION_WORK);
		locValues.add(LOCATION_OTHER);

		Feature location = new FeatureNominal("Location", locValues);

		ArrayList<Feature> features = new ArrayList<Feature>();
		features.add(longitude);
		features.add(latitude);
		features.add(location);

		Signature signature = new Signature(features, features.size() - 1);

		MachineLearningManager mlManager = MachineLearningManager
				.getMLManager(context);

		ClassifierConfig config = new ClassifierConfig();
		config.addParam(si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants.MAX_CLUSTER_DISTANCE,
				Constants.MAX_LOCATION_DISTANCE);
		config.addParam(si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants.MIN_INCLUSION_PERCENT,
				Constants.MIN_LOCATION_PERCENT);
		
		mlManager.addClassifier(
                si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants.TYPE_DENSITY_CLUSTER,
				signature, config,
				Constants.CLASSIFIER_LOCATION);
	}

	// This will be called after every sensing;
	// Before training it gives time-dependent results
	@Override
	public LearnerResultBundle classify(Object... featureValuesO)
			throws ITException, MLException {

		ArrayList<SensorData> allValues = (ArrayList<SensorData>) featureValuesO[0];
		
		SensorData featureValues = allValues.get(0);
		
		if (featureValues.getSensorType() != SensorUtils.SENSOR_TYPE_LOCATION){
			throw new ITException(ITException.INCOMPATIBLE_SENSOR_DATA, 
					"Location data (type "+SensorUtils.SENSOR_TYPE_LOCATION+
					" expected, got type "+featureValues.getSensorType()+" instead.");
		}
		
		LocationData latLon = (LocationData) featureValues;
		Location lastLocation = latLon.getLastLocation();
		if (lastLocation == null) {
			return null;
		}
		
		double latitude = lastLocation.getLatitude();
		double longitude = lastLocation.getLongitude();

		Log.d(TAG, "Classify [" + latitude + "," + longitude + "]");
		// Skip everything if sampling didn't work well
		if (latitude == 0 && longitude == 0) {
			return null;
		}

		Calendar c = Calendar.getInstance();
		int hourOfDay = c.get(Calendar.HOUR_OF_DAY);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		
		String label = LOCATION_OTHER;

		// TODO: check weekend; check charging behaviour
		
		if (dayOfWeek > 1 && dayOfWeek < 7 && 
			hourOfDay >= Constants.MIN_HOUR_WORK && 
			hourOfDay < Constants.MAX_HOUR_WORK) {
			label = LOCATION_WORK;
		} else if (hourOfDay >= Constants.MIN_HOUR_HOME && hourOfDay < Constants.MAX_HOUR_HOME) {
			label = LOCATION_HOME;
		}

		ArrayList<Value> instanceValues = new ArrayList<Value>();

		Value latValue = new Value(latitude, Value.NUMERIC_VALUE);
		Value lonValue = new Value(longitude, Value.NUMERIC_VALUE);
		Value labelValue = new Value(label, Value.NOMINAL_VALUE);

		instanceValues.add(latValue);
		instanceValues.add(lonValue);
		instanceValues.add(labelValue);

		Instance instance = new Instance(instanceValues);

		if (mInstanceQ.size() > Constants.MAX_INSTANCEQ) {
			mInstanceQ.remove(mInstanceQ.entrySet().iterator().next().getKey());
		}
		
		int instanceNumber = mInstanceCounter++;
		mInstanceQ.put(instanceNumber, instance);

		if (!mTrained && mInstanceCounter > Constants.MIN_LOCATION_POINTS){
			trainLearner();
		}
		
		if (!mTrained) {
			Log.d(TAG, "...as " + label + " before training.");
			return new LearnerResultBundle(mType, instanceNumber, mRandom, mTrained,
					latLon, label, instance);
		} else {
			MachineLearningManager mlManager = MachineLearningManager
					.getMLManager(mContext);
			Classifier classifier = mlManager
					.getClassifier(Constants.CLASSIFIER_LOCATION);

			Value v = classifier.classify(instance);
			Log.d(TAG, "...as " + v.getValue() + " after training.");
			return new LearnerResultBundle(mType, instanceNumber, mRandom, mTrained,
					latLon, (String) v.getValue(), instance);
		}

	}

	// For LocationLearner we do batch learning from the recorded instances
	// thus, trainFromFeedback(Object a_class, int a_instanceID) is irrelevant
	// and merely calls trainLearner()
	private void trainLearner() {
		ArrayList<Instance> trainInstances = new ArrayList<Instance>();
		// Make a deep copy here because the classifier manipulates the array
		for (Entry<Integer, Instance> entry : mInstanceQ.entrySet()) {
			trainInstances.add(entry.getValue());
		}
		try {
			MachineLearningManager mlManager = MachineLearningManager
					.getMLManager(mContext);
			Classifier classifier = mlManager
					.getClassifier(Constants.CLASSIFIER_LOCATION);
			classifier.train(trainInstances);
			mTrained = true;
		} catch (MLException e) {
			// Happens if the instances do not comply to the signature
			e.printStackTrace();
		}
	}

	// Train from collected instances
	@Override
	public void trainLearner(Object classValueO, Object... featureValuesO) {
		if (!mTrained) {
			trainLearner();
		}
	}

	// Train from given instances
	@Override
	public void trainLearner(ArrayList<Instance> instances) {
		try {
			MachineLearningManager mlManager = MachineLearningManager
					.getMLManager(mContext);
			Classifier classifier = mlManager
					.getClassifier(Constants.CLASSIFIER_LOCATION);
			classifier.train(instances);
			mTrained = true;
		} catch (MLException e) {
			// Happens if the instances do not comply to the signature
			e.printStackTrace();
		}
	}
	
	// Train from feedback
	@Override
	public void trainFromFeedback(Object classValueO, int instanceID)
			throws MLException {
		if (!mTrained) {
			trainLearner();
		} else {
			// TODO: We need a way to change centroid labels in the classifier,
			// in case the classifier is already trained.
		}
	}

	@Override
	public void printLearnerInfo() {
		Log.d(TAG, "Learner type: " + mType);
		Log.d(TAG, "Sensors used: " + mSensors);
		Log.d(TAG, "Instances queued (size)" + mInstanceQ.size());
		Log.d(TAG, "Instances queued (counter)" + mInstanceCounter);
		Log.d(TAG, "Trained: " + mTrained);
		// d_classifier.printClassifierInfo();
	}

}

/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2015, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package org.ubhave.intelligenttrigger.learners;


import com.ubhave.sensormanager.data.SensorData;

import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;

public class LearnerResultBundle {

	private int mInstanceID;
	private int mLearnerID;
	private Object mValue;
	private boolean mIsRandom;
	private boolean mIsTrained;
	private Instance mInstance;
	private SensorData mSensorData;
	
	// TODO: This is a temporary solution for feeding the result back to the app,
	// in case the app uses sensors outside the interruption library.
	// This is only because subscription + one-off sensing don't work
	// Otherwise, this class would be much simpler:
	/*	public LearnerResultBundle (Object a_value, int a_instanceID, int a_learnerID){
		mInstanceID = a_instanceID;
		mLearnerID = a_learnerID;
		mValue = a_value;
	}*/
	
	public LearnerResultBundle (int learnerID,
			int instanceID,
			boolean isRandom,
			boolean isTrained,
			SensorData sensorData,
			Object value,
			Instance instance){
		mInstanceID = instanceID;
		mIsRandom = isRandom;
		mIsTrained = isTrained;
		mSensorData = sensorData;
		mLearnerID = learnerID;
		mValue = value;
		mInstance = instance;
	}
	
	public Object getValue(){
		return mValue;
	}
	
	public Instance getInstance(){
		return mInstance;
	}
	
	public int getInstanceID(){
		return mInstanceID;
	}
	
	public int getLearnerID(){
		return mLearnerID;
	}
	
	public boolean getIsRandom(){
		return mIsRandom;
	}
	
	public boolean getIsTrained(){
		return mIsTrained;
	}
	
	public SensorData getSensorData(){
		return mSensorData;
	}
}

/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2013, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger.learners;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Random;
import java.util.Map.Entry;

import org.ubhave.intelligenttrigger.utils.Constants;
import org.ubhave.intelligenttrigger.utils.ITException;

import android.content.Context;
import android.util.Log;

import com.ubhave.sensormanager.data.SensorData;
import com.ubhave.sensormanager.data.pullsensor.AccelerometerData;
import com.ubhave.sensormanager.data.pullsensor.LocationData;
import com.ubhave.sensormanager.sensors.SensorUtils;

import si.uni_lj.fri.lrss.machinelearningtoolkit.MachineLearningManager;
import si.uni_lj.fri.lrss.machinelearningtoolkit.classifier.Classifier;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.ClassifierConfig;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Feature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNominal;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNumeric;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Signature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Value;

public class InterruptibilityLearner extends Learner {

	public static final String INTERRUPTIBLE_YES = "yes";
	public static final String INTERRUPTIBLE_NO = "no";

	private static final String TAG = "InterruptibilityLearner";
	
	// Sampled geo coordinates are stored in mLocationInstanceQ
	// together with the label assigned based on the time of the day.
	// Once we have more than Constants.MIN_LOCATION_POINTS the learner
	// calculates clusters of HOME and WORK locations. 
	private int mLocationInstanceCounter;
	private HashMap<Integer, Instance> mLocationInstanceQ;
	private boolean mLocationTrained;
	
	public InterruptibilityLearner() {
		
		mType = Constants.MOD_INTERRUPTIBILITY;
		
		if (mSensors == null) {
			mSensors = new ArrayList<Integer>();
	
			mSensors.add(SensorUtils.SENSOR_TYPE_ACCELEROMETER);
			
			mSensors.add(SensorUtils.SENSOR_TYPE_LOCATION);
		}
		
		if (mInstanceQ == null) {
			
			mInstanceQ = new LinkedHashMap<Integer, Instance>(Constants.MAX_INSTANCEQ);
		
			mInstanceCounter = 0;
		}
		
		if (mLocationInstanceQ == null) {
			
			mLocationInstanceQ = new LinkedHashMap<Integer, Instance>(Constants.MAX_INSTANCEQ);
			
			mLocationInstanceCounter = 0;
		}
	}
	

	@Override
	public void prepareLearner(Context context) throws MLException {
		
		mContext = context;
		
		ArrayList<String> timeOfDayValues = new ArrayList<String>();
		for (int i=0; i<24; i++) {
			timeOfDayValues.add(Integer.toString(i));
		}
		Feature timeOfDay = new FeatureNominal("time_of_day",timeOfDayValues);
		
		ArrayList<String> weekendValues = new ArrayList<String>();
		weekendValues.add("yes");
		weekendValues.add("no");		
		Feature weekendIndicator = new FeatureNominal("weekend", weekendValues);
		//Feature timeIntoExperiment = new Feature("time_into_experiment", Feature.NUMERIC);
		
		Feature accMean = new FeatureNumeric("accMean");
		Feature accVariance = new FeatureNumeric("accVariance");
		Feature accMCR = new FeatureNumeric("accMCR");
		
		// TODO: Do we want to take OTHER into account, or just leave it out as UNKNOWN?
		ArrayList<String> locationValues = new ArrayList<String>();
		locationValues.add(LocationLearner.LOCATION_HOME);
		locationValues.add(LocationLearner.LOCATION_WORK);
		locationValues.add(LocationLearner.LOCATION_OTHER);		
		Feature location = new FeatureNominal("location", locationValues);
		
		ArrayList<String> interValues = new ArrayList<String>();
		interValues.add(INTERRUPTIBLE_YES);
		interValues.add(INTERRUPTIBLE_NO);
		Feature interruptibility = new FeatureNominal("interruptibility", interValues);
					
		ArrayList<Feature> features = new ArrayList<Feature>();		
		features.add(timeOfDay);
		features.add(weekendIndicator);
		//features.add(timeIntoExperiment);
		features.add(accMean);
		features.add(accVariance);
		features.add(accMCR);
		features.add(location);
		features.add(interruptibility);
		Signature signature = new Signature(features, features.size()-1);
		
		MachineLearningManager mlManager = MachineLearningManager.getMLManager(mContext);
		mlManager.addClassifier(
				si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants.TYPE_NAIVE_BAYES,
				signature,
				new ClassifierConfig(),
				Constants.CLASSIFIER_INTERRUPTIBILITY);
		
		// For location classification
		Feature longitude = new FeatureNumeric("longitude");
		Feature latitude = new FeatureNumeric("latitude");

		ArrayList<String> locValues = new ArrayList<String>();
		locValues.add(LocationLearner.LOCATION_HOME);
		locValues.add(LocationLearner.LOCATION_WORK);
		locValues.add(LocationLearner.LOCATION_OTHER);

		Feature locationDesc = new FeatureNominal("location", locValues);

		ArrayList<Feature> featuresLoc = new ArrayList<Feature>();
		featuresLoc.add(longitude);
		featuresLoc.add(latitude);
		featuresLoc.add(locationDesc);

		Signature signatureLoc = new Signature(featuresLoc, featuresLoc.size() - 1);

		ClassifierConfig configLoc = new ClassifierConfig();
		configLoc.addParam(si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants.MAX_CLUSTER_DISTANCE,
				Constants.MAX_LOCATION_DISTANCE);
		configLoc.addParam(si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants.MIN_INCLUSION_PERCENT,
				Constants.MIN_LOCATION_PERCENT);
		
		mlManager.addClassifier(
                si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants.TYPE_DENSITY_CLUSTER,
				signatureLoc, configLoc,
				Constants.CLASSIFIER_LOCATION);
	}
	
	
	@Override
	public LearnerResultBundle classify(Object... featureValuesO) throws ITException, MLException {
		
		Calendar c = Calendar.getInstance();
		
		String weekendIndicator = "no";		
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek == 1 || dayOfWeek == 7) {
			weekendIndicator = "yes";
		}
		
		int hourOfDay = c.get(Calendar.HOUR_OF_DAY);
		
		/*SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mContext);
		long startTime = settings.getLong("start_time", -1);
		int timeIntoExperiment = 0; //TODO: missing value should be put instead
		if (startTime > 0) {
			timeIntoExperiment = (int)((System.currentTimeMillis() - startTime) / (1000*60*60));
		}*/
		
		// TODO: despite the check in SensorTrigger, data is null sometimes?
		if (featureValuesO == null) {
			return null;
		}
		
		ArrayList<SensorData> allSensorsValues = (ArrayList<SensorData>) featureValuesO[0];
		
		// TODO: despite the check in SensorTrigger, data is null sometimes?
		if (allSensorsValues.size() != 2) {
			return null;
		}
		
		SensorData accValues = allSensorsValues.get(0); 

		if (accValues.getSensorType() != SensorUtils.SENSOR_TYPE_ACCELEROMETER){
			throw new ITException(ITException.INCOMPATIBLE_SENSOR_DATA, 
					"Accelerometer data (type "+SensorUtils.SENSOR_TYPE_ACCELEROMETER+
					" expected, got type "+accValues.getSensorType()+" instead.");
		}
				
		ArrayList<float[]> accReadings = ((AccelerometerData)accValues).getSensorReadings();
		
		float accMean = 0;
		float accVariance = 0;
		float accMCR = 0;
		
		float totalIntensity = 0;
		float intensityList[] = new float[accReadings.size()];
				
		for (int i=0; i<accReadings.size(); i++) {
			float[] sample = accReadings.get(i);
			float intensity = (float) Math.sqrt(Math.pow(sample[0], 2) + Math.pow(sample[1], 2) + Math.pow(sample[2], 2));
			intensityList[i] = intensity;
			totalIntensity += intensity;
		}
		if (accReadings.size() > 0) {
			accMean = totalIntensity/accReadings.size();

			for (int i=0; i<accReadings.size(); i++){
				accVariance += Math.pow(intensityList[i] - accMean, 2);
				if (i>0) {
					if ((intensityList[i] - accMean) * (intensityList[i-1] - accMean) < 0){
						accMCR++;
					}
				}
			}

			accVariance = accVariance/accReadings.size();

			if (accReadings.size() > 1) {
				accMCR = accMCR/(accReadings.size()-1);
			}
		}

		
		// Location classifier
		
		String locationLabel = LocationLearner.LOCATION_OTHER;
		
		SensorData locationValues = allSensorsValues.get(1);
		
		if (locationValues.getSensorType() != SensorUtils.SENSOR_TYPE_LOCATION){
			throw new ITException(ITException.INCOMPATIBLE_SENSOR_DATA, 
					"Location data (type "+SensorUtils.SENSOR_TYPE_LOCATION+
					" expected, got type "+locationValues.getSensorType()+" instead.");
		}
		
		LocationData latLon = (LocationData) locationValues;
		if (latLon != null) {
			if (latLon.getLastLocation() != null) {
				double latitude = latLon.getLastLocation().getLatitude();
				double longitude = latLon.getLastLocation().getLongitude();
				//Log.d(TAG, "Classify [" + latitude + "," + longitude + "]");
				// Skip everything if sampling didn't work well
				if (latitude != 0 && longitude != 0) {
					// TODO: check charging behaviour
					if (Constants.WEEKDAYS.contains(dayOfWeek) && 
						hourOfDay >= Constants.MIN_HOUR_WORK && 
						hourOfDay < Constants.MAX_HOUR_WORK) {
						locationLabel = LocationLearner.LOCATION_WORK;
					} else if (hourOfDay >= Constants.MIN_HOUR_HOME && hourOfDay < Constants.MAX_HOUR_HOME) {
						locationLabel = LocationLearner.LOCATION_HOME;
					}	
					
					ArrayList<Value> locationInstanceValues = new ArrayList<Value>();

					Value latValue = new Value(latitude, Value.NUMERIC_VALUE);
					Value lonValue = new Value(longitude, Value.NUMERIC_VALUE);
					
					locationInstanceValues.add(latValue);
					locationInstanceValues.add(lonValue);
					
					Instance locationInstance = new Instance(locationInstanceValues);
					
					if (mLocationTrained) {
						MachineLearningManager mlManager = MachineLearningManager
								.getMLManager(mContext);
						Classifier classifier = mlManager
								.getClassifier(Constants.CLASSIFIER_LOCATION);
						Value classifiedLocationValue = classifier.classify(locationInstance);
						locationLabel = (String) classifiedLocationValue.getValue();
					}
					
					Value locationLabelValue = new Value(locationLabel, Value.NOMINAL_VALUE);
					locationInstance.addValue(locationLabelValue);
					
					int locationInstanceNumber = mLocationInstanceCounter++;
					
					while (mLocationInstanceQ.size() > Constants.MAX_INSTANCEQ) {
						mLocationInstanceQ.remove(mLocationInstanceQ.entrySet().iterator().next().getKey());
					}
					
					mLocationInstanceQ.put(locationInstanceNumber, locationInstance);

					if ((mLocationInstanceCounter > Constants.MIN_LOCATION_POINTS) &&
						!mLocationTrained) {
						ArrayList<Instance> trainInstances = new ArrayList<Instance>();
						// Make a deep copy here because the classifier manipulates the array
						for (Entry<Integer, Instance> entry : mLocationInstanceQ.entrySet()) {
							trainInstances.add(entry.getValue());
						}
						try {
							MachineLearningManager mlManager = MachineLearningManager
									.getMLManager(mContext);
							Classifier classifier = mlManager
									.getClassifier(Constants.CLASSIFIER_LOCATION);
							classifier.train(trainInstances);
							mLocationTrained = true;
						} catch (MLException e) {
							e.printStackTrace();
						}
					}
				}		
			}
		}
		
		
		ArrayList<Value> instanceValues = new ArrayList<Value>();

		Value timeOfDayValue = new Value(Integer.toString(hourOfDay), Value.NOMINAL_VALUE);
		Value weekendValue = new Value(weekendIndicator, Value.NOMINAL_VALUE);
		//Value timeIntoExperimentValue = new Value((float)timeIntoExperiment, Value.NUMERIC_VALUE);
		Value meanValue = new Value((double)accMean, Value.NUMERIC_VALUE);
		Value varianceValue = new Value((double)accVariance, Value.NUMERIC_VALUE);
		Value MCRValue = new Value((double)accMCR, Value.NUMERIC_VALUE);
		// TODO: consider using HOME and WORK only; encode OTHER as a missing value
		Value locationValue = new Value(locationLabel, Value.NOMINAL_VALUE);
		
		instanceValues.add(timeOfDayValue);
		instanceValues.add(weekendValue);
		//instanceValues.add(timeIntoExperimentValue);
		instanceValues.add(meanValue);
		instanceValues.add(varianceValue);
		instanceValues.add(MCRValue);
		instanceValues.add(locationValue);
		
		Instance instance = new Instance(instanceValues);

		while (mInstanceQ.size() > Constants.MAX_INSTANCEQ) {
			mInstanceQ.remove(mInstanceQ.entrySet().iterator().next().getKey());
		}
		
		int instanceNumber = mInstanceCounter++;
		mInstanceQ.put(instanceNumber, instance);
		
		String strValue = INTERRUPTIBLE_NO;
		
		// When we do not have enough interruptibility points, 
		// we turn random triggering on:
		if (mRandom) {
			
			double draw =new Random().nextDouble();
			Log.d(TAG, "Random classification with draw "+draw+" against "+ mRandomProb);
			if (draw < mRandomProb) {
				strValue = INTERRUPTIBLE_YES;
			}
		} else {
			Log.d(TAG, "Intelligent classification");
			MachineLearningManager mlManager = MachineLearningManager.getMLManager(mContext);
			Classifier classifier = mlManager.getClassifier(Constants.CLASSIFIER_INTERRUPTIBILITY);
			Value v = classifier.classify(instance);
			strValue = (String) v.getValue();	
		}
		
		Log.d(TAG, "Accelerometer Mean: "+accMean+"\n"+
				" var: "+accVariance+"\n"+
				" MCR: "+accMCR+"\n"+
				" time of day: "+hourOfDay+"\n"+
				" weekend: "+weekendIndicator+"\n"+
				" location: "+locationLabel+"\n"+
				//" time into experiment: "+timeIntoExperiment+
				" classifed as: "+strValue);
		
		Log.d(TAG, "Result from the learner "+ mType +" for instance "+instanceNumber+ " is "+strValue);
		
		return new LearnerResultBundle(mType, instanceNumber, mRandom, mTrained, accValues, strValue, instance);
	}

	@Override
	public void trainLearner(Object classValueO, Object... featureValuesO) {
		
		// NOTE: time of day is modelled as a nominal value. 
		Value timeOfDayValue = new Value(featureValuesO[0], Value.NOMINAL_VALUE);
		Value weekendValue = new Value(featureValuesO[1], Value.NOMINAL_VALUE);

		// NOTE: time into experiment removed for now.
		// It cannot be modelled as a normally distributed variable, nor nominal		
		// Value timeIntoExperimentValue = new Value(a_featureValues[2], Value.NUMERIC_VALUE);
		Value meanValue = new Value(featureValuesO[3], Value.NUMERIC_VALUE);
		Value varianceValue = new Value(featureValuesO[4], Value.NUMERIC_VALUE);
		Value MCRValue = new Value(featureValuesO[5], Value.NUMERIC_VALUE);
		Value locationValue = new Value(featureValuesO[5], Value.NOMINAL_VALUE);
		Value classValue = new Value(classValueO, Value.NOMINAL_VALUE);
		
		ArrayList<Value> instanceValues = new ArrayList<Value>();
		instanceValues.add(timeOfDayValue);
		instanceValues.add(weekendValue);
		//instanceValues.add(timeIntoExperimentValue);
		instanceValues.add(meanValue);
		instanceValues.add(varianceValue);
		instanceValues.add(MCRValue);
		instanceValues.add(locationValue);
		instanceValues.add(classValue);
		
		Instance instance = new Instance(instanceValues);

		ArrayList<Instance> instances = new ArrayList<Instance>();
		instances.add(instance);
		
		try {
			MachineLearningManager mlManager = MachineLearningManager.getMLManager(mContext);
			Classifier classifier = mlManager.getClassifier(Constants.CLASSIFIER_INTERRUPTIBILITY);
			classifier.train(instances);
			mTrained = true;
		} catch (MLException e) {
			// Happens if the instances do not comply to the signature
			e.printStackTrace();
		}
	}
	
	@Override
	public void trainFromFeedback(Object classValueO, int instanceID) {
		if (mInstanceQ.containsKey(instanceID)) {
			Instance trainInstance = mInstanceQ.get(instanceID);
			Value classValue = new Value(classValueO, Value.NOMINAL_VALUE);
			trainInstance.addValue(classValue);
			ArrayList<Instance> instances = new ArrayList<Instance>();
			instances.add(trainInstance);
			try {
				MachineLearningManager mlManager = MachineLearningManager.getMLManager(mContext);
				Classifier classifier = mlManager.getClassifier(Constants.CLASSIFIER_INTERRUPTIBILITY);
				classifier.train(instances);
				mTrained = true;
			} catch (MLException e) {
				e.printStackTrace();
			}
		}
	}
	

	@Override
	public void trainLearner(ArrayList<Instance> instances) {
		try {
			MachineLearningManager mlManager = MachineLearningManager.getMLManager(mContext);
			Classifier classifier = mlManager.getClassifier(Constants.CLASSIFIER_INTERRUPTIBILITY);
			classifier.train(instances);
			mTrained = true;
		} catch (MLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void printLearnerInfo() {
		Log.d(TAG, "Learner type: "+ mType);
		Log.d(TAG, "Sensors used: "+ mSensors);
		Log.d(TAG, "Instances queued (size) "+ mInstanceQ.size());
		Log.d(TAG, "Instances queued (counter) "+ mInstanceCounter);
		//d_classifier.printClassifierInfo();
	}
}

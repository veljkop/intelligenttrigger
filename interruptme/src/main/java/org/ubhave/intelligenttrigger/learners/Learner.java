/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2015, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger.learners;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.ubhave.intelligenttrigger.utils.ITException;

import android.content.Context;

import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;

public abstract class Learner {

	protected int mType;
	
	transient protected Context mContext;

	// Removed due to serialisation problems
	// protected Classifier d_classifier;
	
	protected List<Integer> mSensors;
	
	protected int mInstanceCounter;
	
	protected LinkedHashMap<Integer, Instance> mInstanceQ;
	
	protected boolean mRandom;
		
	protected double mRandomProb;
	
	boolean mTrained = false;
	
	public abstract void prepareLearner(Context context) throws MLException;
	
	public abstract LearnerResultBundle classify(Object... featureValuesO) throws ITException, MLException;
	
	// Training from a single instance
	public abstract void trainLearner(Object classValueO, Object... featureValuesO);
	
	// Training from a set of instances
	public abstract void trainLearner(ArrayList<Instance> instances);
	
	public abstract void trainFromFeedback(Object classValueO, int instanceID) throws MLException;

	public List<Integer>  usesSensors() {
		return mSensors;
	}
	
	public boolean isRandom(){
		return mRandom;
	}
	
	public boolean isTrained(){
		return mTrained;
	}
	
	public int instanceQueueSize(){
		return mInstanceCounter;
	}
	
	public void randomTriggerSwitch(boolean random, double randomProb) {
		mRandom = random;
		mRandomProb = randomProb;
	}
	
	public abstract void printLearnerInfo();
}

/*
 *  Copyright (c) 2013, University of Birmingham, UK,
 *  Copyright (c) 2015, University of Ljubljana, Slovenia,
 *  Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>
 *
 *
 *  This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
 *  Computing for Positive Behaviour Change) Project. For more information, please visit
 *  http://www.ubhave.org
 *
 *  Permission to use, copy, modify, and/or distribute this software for any purpose with
 *  or without fee is hereby granted, provided that the above copyright notice and this
 *  permission notice appear in all copies.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package org.ubhave.intelligenttrigger;

import java.util.ArrayList;

import org.ubhave.intelligenttrigger.learners.LearnerResultBundle;

/**
 * Implement this interface in a class that needs to be notified when a trigger fires.
 * 
 * @author Veljko Pejovic, University of Birmingham, UK <v.pejovic@cs.bham.ac.uk>
 */

public interface IntelligentTriggerReceiver {

	/**
	 * Fired when trigger conditions are satisfied. The learner result bundles include
	 * IDs of (sensed) instance(s) that resulted in the trigger conditions being satisfied.  
	 * These IDs can be returned to the IntelligentTriggerManager together with labels 
	 * provided by the overlying application so that learners can be trained.
	 * 
	 * @param triggerID A string trigger ID.
	 * @param bundles Information about instances that lead to the notification event.
	 */
	public void onTriggerReceived(String triggerID, ArrayList<LearnerResultBundle> bundles);
}

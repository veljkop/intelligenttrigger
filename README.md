#IntelligentTrigger
======================

IntelligentTrigger (a.k.a. InterruptMe) is an Android library that detects suitable
moments to interrupt a user.  It is designed to make interactive tasks less burdensome.
The library hosts a learner, i.e. a model of user's interruptibility. It  takes into account
user's physical activity (accelerometer features), location and time of the day / week,
in order to infer interruptibility. The learner is trained either from the user feedback
directly, from data gathered in a text file, or from a set of data instances (as Java objects).

Note that the library only notifies the overlying listener about a good moment to interrupt.
It is on the application to implement the actual means of notifying the user and presenting
 the relevant data at that point.

Finally, in addition to learning about good moments to interrupt, the library can notify
the listener about times when a specific location (home, work) is detected, or when a level
of user activity (active, not active) is detected.

To use IntelligentTrigger in your Android project, simply add the following line to dependencies
within your app's build.gradle file:

    compile 'compile 'org.ubhave.intelligenttrigger:interruptme:1.0' 

Please cite: V. Pejovic and M. Musolesi, "InterruptMe: Designing Intelligent Prompting
Mechanisms for Pervasive Applications", UbiComp'14, Seattle, WA, USA, September 2014.
if using this library.

https://bitbucket.org/veljkop/intelligenttrigger


IntelligentTrigger relies on the following third-party libraries:

* Machine learning toolkit: https://github.com/vpejovic/MachineLearningToolkit
* ES Sensor Manager: https://github.com/nlathia/SensorManager
* ES Trigger Manager: https://github.com/nlathia/TriggerManager

Individual licenses are available in LICENSE-3RD-PARTY.txt


Copyright (c) 2013, University of Birmingham, UK
Copyright (c) 2015, University of Ljubljana, Slovenia

Veljko Pejovic,  <Veljko.Pejovic@fri.uni-lj.si>


This library was developed as part of the EPSRC Ubhave (Ubiquitous and Social
Computing for Positive Behaviour Change) Project. For more information, please visit
http://www.ubhave.org

Permission to use, copy, modify, and/or distribute this software for any purpose with
or without fee is hereby granted, provided that the above copyright notice and this
permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
